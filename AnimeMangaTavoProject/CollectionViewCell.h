//
//  CollectionViewCell.h
//  AnimeMangaTavoProject
//
//  Created by tavo7910 on 4/10/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *animeImageView;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@end
