//
//  TAVAnimeDetailViewController.m
//  AnimeMangaTavoProject
//
//  Created by tavo7910 on 4/10/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import "TAVAnimeDetailViewController.h"
#import "TAVPhotoInstance.h"

@interface TAVAnimeDetailViewController ()

@end

@implementation TAVAnimeDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fillDeatil];
}

- (void)fillDeatil{
    self.titleLabel.text =self.seriesModel.title;
    NSURL *url = self.seriesModel.imageBanner;
    [self.detailImageAnimeView sd_setImageWithURL:url placeholderImage:nil options:SDWebImageRefreshCached];
    
    NSString *seriesType = self.seriesModel.seriesType;
    NSString *genres = self.seriesModel.genres;
    NSInteger averageScore = self.seriesModel.averageScore;
    NSInteger totalEpisodes = self.seriesModel.totalEpisodes;
    NSInteger totalChapters = self.seriesModel.totalChapters;
    if (totalEpisodes == 0) {
        totalEpisodes = totalChapters;
    }
    self.typeLabel.text = [NSString stringWithFormat:@"GENRES\n%@ \nTYPE \n%@ \n\nSCORE \n%li \n\nEPISODES \n%li",genres,seriesType,averageScore, totalEpisodes];
}

@end
