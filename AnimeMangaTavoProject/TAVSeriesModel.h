//
//  TAVSeriesModel.h
//  AnimeMangaTavoProject
//
//  Created by tavo7910 on 4/10/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TAVSeriesModel : NSObject
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *seriesType;
@property (strong, nonatomic) NSString *genres;
@property (assign, nonatomic) NSInteger averageScore;
@property (assign, nonatomic) NSInteger totalEpisodes;
@property (assign, nonatomic) NSInteger totalChapters;
@property (strong, nonatomic) NSURL *imageSmall;
@property (strong, nonatomic) NSURL *imageLarge;
@property (strong, nonatomic) NSURL *imageBanner;

- (instancetype)initWithTitle:(NSString *)title seriestype:(NSString *)seriesType genres:(NSString *)genres averageScore:(NSInteger)averageScore totalEpisodes:(NSInteger)totalEpisodes totalChapters:(NSInteger)totalChapters imageSmall:(NSURL *)imageSmall imageLarge:(NSURL *)imageLarge imageBanner:(NSURL *)imageBanner;

@end
