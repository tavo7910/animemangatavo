//
//  TAVAnimeDetailViewController.h
//  AnimeMangaTavoProject
//
//  Created by tavo7910 on 4/10/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TAVSeriesModel.h"

@interface TAVAnimeDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *detailImageAnimeView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;

@property (strong, nonatomic) TAVSeriesModel *seriesModel;

@end
