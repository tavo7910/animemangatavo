//
//  TAVDataFetchModel.h
//  AnimeMangaTavoProject
//
//  Created by tavo7910 on 4/10/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TAVDataFetchModel : NSObject

- (void)getData:(void (^)(NSArray *seriesDataArray))success;
- (void)getDataForManga:(void (^)(NSArray *seriesDataArray))success;
- (NSString *)mangaURLString;

@end
