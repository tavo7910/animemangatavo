//
//  TAVNetworkParser.m
//  AnimeMangaTavoProject
//
//  Created by tavo7910 on 4/10/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import "TAVNetworkParser.h"
#import <AFNetworking/AFNetworking.h>

@interface TAVNetworkParser ()
@property (strong, nonatomic) AFHTTPSessionManager *manager;
@end

@implementation TAVNetworkParser

- (AFHTTPSessionManager *)manager {
    if (!_manager) {
        _manager = [AFHTTPSessionManager manager];
        _manager.responseSerializer = [AFJSONResponseSerializer new];
    }
    return _manager;
}

- (void)getResponse:(NSURL *)url successBlock:(void (^)(NSArray *responseArray))success {

    [self.manager GET:url.absoluteString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error to download");
    }];
    
}



@end



//-------------------------------------------------------------------------------------------------------------------------------
//This coments are the manual form to ask a JSON
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url
//                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                if (error) {
//                                                    NSLog(@"error to download");
//                                                }
//                                                else {
//                                                    id objectJson  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//                                                    NSArray *json = objectJson;
//                                                    dispatch_async(dispatch_get_main_queue(), ^{
//                                                        if (success) {
//                                                            success(json);
//                                                        }
//
//
//                                                    });
//
//
//                                                }
//                                            }];
//    [dataTask resume];
