//
//  TAVPost.m
//  AnimeMangaTavoProject
//
//  Created by tavo7910 on 4/10/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import "TAVPost.h"
#import "TAVNetworkParser.h"
#import "TAVSeriesModel.h"
#import "TAVKey.h"

#import <AFNetworking/AFNetworking.h>
#import <AFOAuth2Manager.h>

@interface TAVPost()

@property (strong, nonatomic) TAVNetworkParser *TAVNetworkParser;
@property (nonatomic) NSTimeInterval validTokenTime;
@property (strong, nonatomic) NSString *validTokenKey;
@property (strong, nonatomic) NSArray *seriesDataArray;
@property (copy, nonatomic) NSString *mangaURLString;
@property (strong, nonatomic) NSMutableArray *peopleModelMutableArray;
@end

@implementation TAVPost


- (void)postURL:(NSURL *)url successBlock:(void (^)(NSArray *responseArray))success {
    
    if([url.absoluteString isEqualToString:@"https://anilist.co/api/browse/manga?access_token="]){
        NSURL *mangaURLAux = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",url.absoluteString,self.validTokenKey]];
        [self.TAVNetworkParser getResponse:mangaURLAux successBlock:^(NSArray *responseArray) {
            success(responseArray);
        }];
        
    } else{
    
        NSURL *baseURL = [NSURL URLWithString:@"https://anilist.co/api/"];
        AFOAuth2Manager *OAuth2Manager = [[AFOAuth2Manager alloc] initWithBaseURL:baseURL
                                                                         clientID:clientID
                                                                           secret:clientSecret];
        
        [OAuth2Manager setResponseSerializer:[AFJSONResponseSerializer new]];
        
        NSDictionary *params = @{@"grant_type": grantType,
                                 @"client_id": clientID,
                                 @"client_secret": clientSecret
                                 };
        
        NSTimeInterval timeNow = [[NSDate date] timeIntervalSince1970];
        self.validTokenTime = timeNow;
        if (timeNow < self.validTokenTime + 3600) {
            [OAuth2Manager authenticateUsingOAuthWithURLString:@"auth/access_token" parameters:params success:^(AFOAuthCredential * _Nonnull credential) {
                NSLog(@"JSON: %@",credential);
                NSUserDefaults *token = [[NSUserDefaults alloc]init];
                [token setObject:credential.accessToken forKey:@"keyToken"];
                self.validTokenKey = credential.accessToken;
                NSString *urlAnilistString = self.validTokenKey;
                NSString *urlANimeString = [NSString stringWithFormat:@"%@%@",url,urlAnilistString];
                NSURL *urlNet = [NSURL URLWithString:urlANimeString];
                [self.TAVNetworkParser getResponse:urlNet successBlock:^(NSArray *responseArray) {
                    success(responseArray);
                }];
            } failure:^(NSError * _Nonnull error) {
                NSLog(@"ERROR %@", error);
            }];
            
            self.validTokenTime = timeNow;
        }

    }
    
}



- (void)createModel{
    self.peopleModelMutableArray = [NSMutableArray new];
    for(NSDictionary *responseDict in self.seriesDataArray){
        NSString *title = responseDict[@"title_english"];
        NSString *seriesType = responseDict[@"series_type"];
        //to get the information about genres
        NSString *genresIntoFor = @"";
        NSArray *genresArray = responseDict[@"genres"];
        for(NSString *aux in genresArray){
            genresIntoFor = [NSString stringWithFormat:@"%@%@, ",genresIntoFor,aux];
        } //end of genres
        NSString *genres = genresIntoFor;
        NSInteger averageScore =[responseDict[@"average_score"] integerValue];
        NSInteger totalEpisodes =[responseDict[@"total_episodes"] integerValue];
        NSInteger totalChapters = [responseDict[@"total_chapters"] integerValue];
        NSURL *imageSmall = responseDict[@"image_url_sml"];
        NSURL *imageLarge = [NSURL URLWithString:responseDict[@"image_url_lge"]];
        NSURL *imageBanner = [NSURL URLWithString:responseDict[@"image_url_banner"]];
        TAVSeriesModel *model = [[TAVSeriesModel alloc]initWithTitle:title seriestype:seriesType genres:genres averageScore:averageScore totalEpisodes:totalEpisodes totalChapters:totalChapters imageSmall:imageSmall imageLarge:imageLarge imageBanner:imageBanner];
        [self.peopleModelMutableArray addObject:model];
        
    }
}

- (TAVNetworkParser *)TAVNetworkParser{
    if(!_TAVNetworkParser){
        _TAVNetworkParser = [[TAVNetworkParser alloc]init];
    }
    return _TAVNetworkParser;
}

@end



//    } else {
//        NSString *post = [NSString stringWithFormat:@"grant_type=%@&client_id=%@&client_secret=%@",grantType,clientID,clientSecret];
//        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//
//        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
//
//        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//        [request setURL:[NSURL URLWithString:@"https://anilist.co/api/auth/access_token"]];
//        [request setHTTPMethod:@"POST"];
//        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
//        [request setHTTPBody:postData];
//
//        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//        [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//            NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
//            NSLog(@"requestReply: %@", requestReply);
//            id objectJson  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//            dispatch_async(dispatch_get_main_queue(), ^{
//
//                self.validTokenTime = [objectJson[@"expires"]doubleValue];
//                self.validTokenKey = objectJson[@"access_token"];
//                NSString *urlAnilistString = self.validTokenKey;
//                NSString *urlANimeString = [NSString stringWithFormat:@"%@%@",url,urlAnilistString];
//                NSURL *urlNet = [NSURL URLWithString:urlANimeString];
//                [self.TAVNetworkParser getResponse:urlNet successBlock:^(NSArray *responseArray) {
//                    success(responseArray);
//                }];
//
//            });
//
//        }]
//         resume];
//
//    }



//
//        NSString *post = [NSString stringWithFormat:@"grant_type=%@&client_id=%@&client_secret=%@",grantType,clientID,clientSecret];
//        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//
//        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
//
//        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//        [request setURL:[NSURL URLWithString:@"https://anilist.co/api/auth/access_token"]];
//        [request setHTTPMethod:@"POST"];
//        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
//        [request setHTTPBody:postData];
//
//        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//        [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//            NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
//            NSLog(@"requestReply: %@", requestReply);
//            id objectJson  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//            dispatch_async(dispatch_get_main_queue(), ^{
//
//                self.validTokenTime = [objectJson[@"expires"]doubleValue];
//                self.validTokenKey = objectJson[@"access_token"];
//                NSString *urlAnilistString = self.validTokenKey;
//                NSString *urlANimeString = [NSString stringWithFormat:@"%@%@",url,urlAnilistString];
//                NSURL *urlNet = [NSURL URLWithString:urlANimeString];
//                [self.TAVNetworkParser getResponse:urlNet successBlock:^(NSArray *responseArray) {
//                    success(responseArray);
//                }];
//
//            });
//
//        }]
//         resume];
//    }
