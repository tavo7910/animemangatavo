//
//  AnimeMangaCollectionViewController.m
//  AnimeMangaTavoProject
//
//  Created by tavo7910 on 4/10/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import "AnimeMangaCollectionViewController.h"
#import "CollectionViewCell.h"
#import "TAVAnimeDetailViewController.h"
#import "TAVDataFetchModel.h"
#import "TAVSeriesModel.h"
#import "TAVPhotoInstance.h"
#import "TAVPost.h"

@interface AnimeMangaCollectionViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UISearchResultsUpdating>
@property (strong, nonatomic) TAVDataFetchModel *model;
@property (strong, nonatomic) TAVPost *postURL;
@property (strong, nonatomic) NSArray *peopleArray;
@property (strong, nonatomic) NSArray *originalDataArray;
@property (strong, nonatomic) UISearchController *barController;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation AnimeMangaCollectionViewController

static NSString * const reuseIdentifier = @"AnimeCellID";

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fetchData];
    [self searchController];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showAnimeDetail"]) {
        NSIndexPath *indexPath = [self.collectionView indexPathForCell:sender];
        TAVAnimeDetailViewController *controller = (TAVAnimeDetailViewController *)[segue destinationViewController];
        controller.seriesModel = self.peopleArray[indexPath.row];
        controller.titleLabel.text = controller.seriesModel.title;
    }
    
}

#pragma mark - Methods

- (void)searchController{
    self.barController.searchResultsUpdater = self;
    self.barController.dimsBackgroundDuringPresentation = NO;
    self.definesPresentationContext = YES;
    self.navigationItem.titleView = self.barController.searchBar;
    self.barController.hidesNavigationBarDuringPresentation = NO;
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    NSString *searchString = searchController.searchBar.text;
    
    if (searchString.length) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title contains[c] %@", searchString];
        NSArray *results = [self.originalDataArray filteredArrayUsingPredicate:predicate];
        if (results.count) {
            self.peopleArray = results;
        }
    }
    else {
        self.peopleArray = self.originalDataArray;
    }
    [self.collectionView reloadData];
}

- (void)fetchData{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UITabBarController *rootViewController = (UITabBarController *)window.rootViewController;
    UINavigationController *nav = (UINavigationController *)[rootViewController.viewControllers lastObject];
    if (self ==  [nav topViewController]) {
        [self.model getDataForManga:^(NSArray *peopleDataArray) {
            NSMutableArray *ret = [NSMutableArray arrayWithCapacity:[self.peopleArray count] + [peopleDataArray count]];
            [ret addObjectsFromArray:self.peopleArray];
            [ret addObjectsFromArray:peopleDataArray];
            self.peopleArray = ret;
            [self.collectionView reloadData];
        }];
    }
    else {
        [self.model getData:^(NSArray *peopleDataArray) {
            
            NSMutableArray *ret = [NSMutableArray arrayWithCapacity:[self.peopleArray count] + [peopleDataArray count]];
            [ret addObjectsFromArray:self.peopleArray];
            [ret addObjectsFromArray:peopleDataArray];
            self.peopleArray = ret;
            [self.collectionView reloadData];
            [self.activityIndicator stopAnimating];
        }];
    }
    
    
}

#pragma mark - Lazy

-(NSArray *)originalDataArray{
    if (!_originalDataArray) {
        _originalDataArray = [self.peopleArray copy];
    }
    return _originalDataArray;
}

- (UISearchController *)barController{
    if(!_barController){
        _barController = [[UISearchController alloc]initWithSearchResultsController:nil];
        
    }
    
    return _barController;
}

- (TAVDataFetchModel *)model {
    if (!_model) {
        _model= [[TAVDataFetchModel alloc]init];
    }
    return _model;
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.peopleArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    NSString *title = [self.peopleArray[indexPath.row] title];
    NSURL *ImageURL = [self.peopleArray[indexPath.row] imageLarge];
    [cell.animeImageView sd_setImageWithURL:ImageURL];
    cell.textLabel.text = title;
//    
//    if (indexPath.row == self.peopleArray.count-1 && !([self.model.mangaURLString isEqualToString:@""])) {
//        [self.activityIndicator startAnimating];
//        [self fetchData];
//    }
    return cell;
}

#pragma mark <UICollectionViewDelegate>

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [self.collectionView.collectionViewLayout invalidateLayout];
    
}

- (CGFloat )collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0f;
}

- (CGFloat )collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0f;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize cellSize;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        cellSize.width = ([UIScreen mainScreen].bounds.size.width)/6;
        
    }
    else {
        cellSize.width = ([UIScreen mainScreen].bounds.size.width)/2;
        
    }
    cellSize.height =  cellSize.width;
    return cellSize;
}

@end
