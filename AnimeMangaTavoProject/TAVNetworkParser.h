//
//  TAVNetworkParser.h
//  AnimeMangaTavoProject
//
//  Created by tavo7910 on 4/10/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TAVNetworkParser : NSObject

- (void)getResponse:(NSURL *)url successBlock:(void (^)(NSArray *responseArray))success;

@end
