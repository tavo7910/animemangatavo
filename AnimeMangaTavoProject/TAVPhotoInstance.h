//
//  TAVPhotoInstance.h
//  AnimeMangaTavoProject
//
//  Created by tavo7910 on 4/10/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface TAVPhotoInstance : NSObject

+ (id)sharedInstance;

- (UIImage *)getImage:(NSURL *)imageURL completion :(void (^)(UIImage *image))success;

@end
