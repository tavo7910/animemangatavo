//
//  TAVDataFetchModel.m
//  AnimeMangaTavoProject
//
//  Created by tavo7910 on 4/10/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import "TAVNetworkParser.h"
#import "TAVDataFetchModel.h"
#import "TAVSeriesModel.h"
#import "TAVPost.h"

@interface TAVDataFetchModel()
@property (strong, nonatomic) TAVNetworkParser *dataResponse;
@property (strong, nonatomic) TAVPost *postToConexion;
@property (strong, nonatomic) NSArray *seriesDataArray;
@property (copy, nonatomic) NSString *mangaURLString;
@property (strong, nonatomic) NSMutableArray *peopleModelMutableArray;
@property (assign,nonatomic) NSInteger aux;
@end

static NSString *const animeURLString = @"https://anilist.co/api/browse/anime?access_token=";
static NSString *const mangaURLString =@"https://anilist.co/api/browse/manga?access_token=";

@implementation TAVDataFetchModel

#pragma mark - Lazy

- (TAVPost *)postToConexion{
    if (!_postToConexion) {
        _postToConexion = [[TAVPost alloc]init];
        
    }
    return _postToConexion;
}

- (NSArray *)seriesDataArray{
    if (!_seriesDataArray) {
        _seriesDataArray = [[NSArray alloc]init];
        
    }
    return _seriesDataArray;
}

- (NSMutableArray *)peopleModelMutableArray{
    if (!_peopleModelMutableArray) {
        _peopleModelMutableArray= [[NSMutableArray alloc]init];
        
    }
    return _peopleModelMutableArray;
}

- (TAVNetworkParser *)dataResponse{
    if (!_dataResponse) {
        _dataResponse= [[TAVNetworkParser alloc]init];
        
    }
    return _dataResponse;
}

#pragma mark - Methods

- (void)createModel {
    self.peopleModelMutableArray = [NSMutableArray new];
    for(NSDictionary *responseDict in self.seriesDataArray) {
        NSString *title = responseDict[@"title_english"];
        NSString *seriesType = responseDict[@"series_type"];
        //to get the information about genres
        NSString *genresIntoFor = @"";
        NSArray *genresArray = responseDict[@"genres"];
        for(NSString *aux in genresArray) {
            genresIntoFor = [NSString stringWithFormat:@"%@%@\n",genresIntoFor,aux];
        } //end of genres
        NSString *genres = genresIntoFor;
        NSInteger averageScore =[responseDict[@"average_score"] integerValue];
        NSInteger totalEpisodes =[responseDict[@"total_episodes"] integerValue];
        NSInteger totalChapters = [responseDict[@"total_chapters"] integerValue];
        NSURL *imageSmall = responseDict[@"image_url_sml"];
        NSURL *imageLarge = [NSURL URLWithString:responseDict[@"image_url_lge"]];
        NSURL *imageBanner = nil;
        if (responseDict[@"image_url_banner"] != (id)[NSNull null]) {
            imageBanner = [NSURL URLWithString:responseDict[@"image_url_banner"]];
            
        }
        else {
            imageBanner = [NSURL URLWithString:responseDict[@"image_url_lge"]];
            
        }
        TAVSeriesModel *model = [[TAVSeriesModel alloc]initWithTitle:title seriestype:seriesType genres:genres averageScore:averageScore totalEpisodes:totalEpisodes totalChapters:totalChapters imageSmall:imageSmall imageLarge:imageLarge imageBanner:imageBanner];
        [self.peopleModelMutableArray addObject:model];
        
    }
}

- (void)getData:(void (^)(NSArray *seriesDataArray))success{
    
    NSURL *url =nil;
    url = [[NSURL alloc]initWithString:animeURLString];
    
    [self.postToConexion postURL:url successBlock:^(NSArray *responseArray) {
        self.seriesDataArray = responseArray;
        [self createModel];
        success(self.peopleModelMutableArray);
    }];
    
}

- (void)getDataForManga:(void (^)(NSArray *seriesDataArray))success{
    
    NSURL *url =nil;
    url = [[NSURL alloc]initWithString:mangaURLString];
    [self.postToConexion postURL:url successBlock:^(NSArray *responseArray) {
        self.seriesDataArray = responseArray;
        [self createModel];
        success(self.peopleModelMutableArray);
    }];
    
}

@end
