//
//  TAVSeriesModel.m
//  AnimeMangaTavoProject
//
//  Created by tavo7910 on 4/10/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import "TAVSeriesModel.h"

@implementation TAVSeriesModel

- (instancetype)initWithTitle:(NSString *)title seriestype:(NSString *)seriesType genres:(NSString *)genres averageScore:(NSInteger)averageScore totalEpisodes:(NSInteger)totalEpisodes totalChapters:(NSInteger)totalChapters imageSmall:(NSURL *)imageSmall imageLarge:(NSURL *)imageLarge imageBanner:(NSURL *)imageBanner{
    self = [super init];
    if (self){
        _title = title;
        _seriesType = seriesType;
        _genres = genres;
        _averageScore = averageScore;
        _totalEpisodes = totalEpisodes;
        _totalChapters = totalChapters;
        _imageSmall = imageSmall;
        _imageLarge = imageLarge;
        _imageBanner = imageBanner;
    }
    return self;
}

@end
