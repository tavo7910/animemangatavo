//
//  TAVPhotoInstance.m
//  AnimeMangaTavoProject
//
//  Created by tavo7910 on 4/10/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import "TAVPhotoInstance.h"
#import "TAVNetworkParser.h"

@implementation TAVPhotoInstance

#pragma mark Singleton Methods

+ (id)sharedInstance {
    static TAVPhotoInstance *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    if (self = [super init]) {
    }
    return self;
}

- (NSString *)getCacheDirectoryPath {
    NSArray *array = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    return [array firstObject];
}

- (void)saveImage:(UIImage *)img withName:(NSString *)name {
    NSData *data =  UIImageJPEGRepresentation(img, 1.0);
    NSString *imagePath =[NSString stringWithFormat:@"%@/%@",[self getCacheDirectoryPath],name];
    
    if ([[NSFileManager defaultManager]fileExistsAtPath:imagePath]) {
        [[NSFileManager defaultManager]removeItemAtPath:imagePath error:nil];
        
    }
    [data writeToFile:imagePath atomically:YES];
}


- (UIImage *)getImageFromCacheWithName:(NSString *)name {
    NSString *imagePath =[NSString stringWithFormat:@"%@/%@",[self getCacheDirectoryPath],name];
    if ([[NSFileManager defaultManager]fileExistsAtPath:imagePath]) {
        UIImage *image =[UIImage imageWithContentsOfFile:imagePath];
        return image;
        
    }
    return nil;
}

- (UIImage *)getImage:(NSURL *)imageURL completion :(void (^)(UIImage *image))success {
    NSString *imageURLString = [imageURL absoluteString];
    NSArray *partsOfURL = [imageURLString componentsSeparatedByString:@"/"];
    NSString *fileName = [partsOfURL lastObject];
    UIImage *obtainedImage = [self getImageFromCacheWithName:fileName];
    if (!obtainedImage) {
        __weak typeof (self) weakSelf = self;
        [self getRemoteImage:imageURL completion:^(UIImage *image) {
            if (image) {
                [weakSelf saveImage:image withName:fileName];
                
            }
            if (success) {
                success(image);
                
            }
        }];
    }
    else {
        if (success) {
            success(obtainedImage);
            
        }
    }
    return obtainedImage;
}

- (void)getRemoteImage:(NSURL *)imageURL completion :(void (^)(UIImage *image))success {
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:imageURL completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (success) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    success(image);
                    
                });
            }
        }
        else{
            NSLog(@"error: %@",error);
            
        }
    }];
    [task resume];
}

@end
